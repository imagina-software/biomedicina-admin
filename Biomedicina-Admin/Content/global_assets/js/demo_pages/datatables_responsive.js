/* ------------------------------------------------------------------------------
 *
 *  # Responsive extension for Datatables
 *
 *  Demo JS code for datatable_responsive.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var DatatableResponsive = function() {


    //
    // Setup module components
    //

    // Basic Datatable examples
    var _componentDatatableResponsiveUsuario = function() {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            columnDefs: [{ 
                orderable: false,
                width: 100,
                targets: [ 6 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });


        // Basic responsive configuration


        //$('.datatable-responsive').DataTable();
        $('#table-users').DataTable();



        // Column controlled child rows
        //$('.datatable-responsive-column-controlled').DataTable({
        //    responsive: {
        //        details: {
        //            type: 'column'
        //        }
        //    },
        //    columnDefs: [
        //        {
        //            className: 'control',
        //            orderable: false,
        //            targets:   0
        //        },
        //        { 
        //            width: "100px",
        //            targets: [6]
        //        },
        //        { 
        //            orderable: false,
        //            targets: [6]
        //        }
        //    ],
        //    order: [1, 'asc']
        //});



    };
    var _componentDatatableResponsiveCliente = function () {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            columnDefs: [{
                orderable: false,
                width: 100,
                targets: [6]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });


        // Basic responsive configuration


        //$('.datatable-responsive').DataTable();
        $('#table-clientes').DataTable();



        // Column controlled child rows
        //$('.datatable-responsive-column-controlled').DataTable({
        //    responsive: {
        //        details: {
        //            type: 'column'
        //        }
        //    },
        //    columnDefs: [
        //        {
        //            className: 'control',
        //            orderable: false,
        //            targets:   0
        //        },
        //        { 
        //            width: "100px",
        //            targets: [6]
        //        },
        //        { 
        //            orderable: false,
        //            targets: [6]
        //        }
        //    ],
        //    order: [1, 'asc']
        //});



    };
    var _componentDatatableResponsiveProducto = function () {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            columnDefs: [{
                orderable: false,
                width: 100,
                targets: [6]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });


        // Basic responsive configuration


        //$('.datatable-responsive').DataTable();
        $('#table-productos').DataTable();



        // Column controlled child rows
        //$('.datatable-responsive-column-controlled').DataTable({
        //    responsive: {
        //        details: {
        //            type: 'column'
        //        }
        //    },
        //    columnDefs: [
        //        {
        //            className: 'control',
        //            orderable: false,
        //            targets:   0
        //        },
        //        { 
        //            width: "100px",
        //            targets: [6]
        //        },
        //        { 
        //            orderable: false,
        //            targets: [6]
        //        }
        //    ],
        //    order: [1, 'asc']
        //});



    };
    var _componentDatatableResponsiveEmpresa = function () {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            columnDefs: [{
                orderable: false,
                width: 100,
                targets: [7]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });


        // Basic responsive configuration


        //$('.datatable-responsive').DataTable();
        $('#table-empresas').DataTable();



        // Column controlled child rows
        //$('.datatable-responsive-column-controlled').DataTable({
        //    responsive: {
        //        details: {
        //            type: 'column'
        //        }
        //    },
        //    columnDefs: [
        //        {
        //            className: 'control',
        //            orderable: false,
        //            targets:   0
        //        },
        //        { 
        //            width: "100px",
        //            targets: [6]
        //        },
        //        { 
        //            orderable: false,
        //            targets: [6]
        //        }
        //    ],
        //    order: [1, 'asc']
        //});



    };
    var _componentDatatableResponsiveCategoriaPro = function () {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            columnDefs: [{
                orderable: false,
                width: 100,
                targets: [3]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });


        // Basic responsive configuration


        //$('.datatable-responsive').DataTable();
        $('#table-catPro').DataTable();



        // Column controlled child rows
        //$('.datatable-responsive-column-controlled').DataTable({
        //    responsive: {
        //        details: {
        //            type: 'column'
        //        }
        //    },
        //    columnDefs: [
        //        {
        //            className: 'control',
        //            orderable: false,
        //            targets:   0
        //        },
        //        { 
        //            width: "100px",
        //            targets: [6]
        //        },
        //        { 
        //            orderable: false,
        //            targets: [6]
        //        }
        //    ],
        //    order: [1, 'asc']
        //});



    };





    // Select2 for length menu styling
    var _componentSelect2 = function() {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentDatatableResponsiveUsuario();
            _componentDatatableResponsiveCliente();
            _componentDatatableResponsiveProducto();
            _componentDatatableResponsiveEmpresa();
            _componentDatatableResponsiveCategoriaPro();
            _componentSelect2();
        }
    }
}();
//var DatatableResponsiveClientes = function () {


//    //
//    // Setup module components
//    //

//    // Basic Datatable examples
//    var _componentDatatableResponsive = function () {
//        if (!$().DataTable) {
//            console.warn('Warning - datatables.min.js is not loaded.');
//            return;
//        }

//        // Setting datatable defaults
//        $.extend($.fn.dataTable.defaults, {
//            autoWidth: false,
//            responsive: true,
//            columnDefs: [{
//                orderable: false,
//                width: 100,
//                targets: [6]
//            }],
//            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
//            language: {
//                search: '<span>Filter:</span> _INPUT_',
//                searchPlaceholder: 'Type to filter...',
//                lengthMenu: '<span>Show:</span> _MENU_',
//                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
//            }
//        });


//        // Basic responsive configuration


//        //$('.datatable-responsive').DataTable();
//        $('#table-clientes').DataTable();



//        // Column controlled child rows
//        $('.datatable-responsive-column-controlled').DataTable({
//            responsive: {
//                details: {
//                    type: 'column'
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: 0
//                },
//                {
//                    width: "100px",
//                    targets: [6]
//                },
//                {
//                    orderable: false,
//                    targets: [6]
//                }
//            ],
//            order: [1, 'asc']
//        });


//        // Control position
//        $('.datatable-responsive-control-right').DataTable({
//            responsive: {
//                details: {
//                    type: 'column',
//                    target: -1
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: -1
//                },
//                {
//                    width: "100px",
//                    targets: [5]
//                },
//                {
//                    orderable: false,
//                    targets: [5]
//                }
//            ]
//        });


//        // Whole row as a control
//        $('.datatable-responsive-row-control').DataTable({
//            responsive: {
//                details: {
//                    type: 'column',
//                    target: 'tr'
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: 0
//                },
//                {
//                    width: "100px",
//                    targets: [6]
//                },
//                {
//                    orderable: false,
//                    targets: [6]
//                }
//            ],
//            order: [1, 'asc']
//        });
//    };

//    // Select2 for length menu styling
//    var _componentSelect2 = function () {
//        if (!$().select2) {
//            console.warn('Warning - select2.min.js is not loaded.');
//            return;
//        }

//        // Initialize
//        $('.dataTables_length select').select2({
//            minimumResultsForSearch: Infinity,
//            dropdownAutoWidth: true,
//            width: 'auto'
//        });
//    };


//    //
//    // Return objects assigned to module
//    //

//    return {
//        init: function () {
//            _componentDatatableResponsive();
//            _componentSelect2();
//        }
//    }
//}();
//var DatatableResponsiveProductos = function () {


//    //
//    // Setup module components
//    //

//    // Basic Datatable examples
//    var _componentDatatableResponsive = function () {
//        if (!$().DataTable) {
//            console.warn('Warning - datatables.min.js is not loaded.');
//            return;
//        }

//        // Setting datatable defaults
//        $.extend($.fn.dataTable.defaults, {
//            autoWidth: false,
//            responsive: true,
//            columnDefs: [{
//                orderable: false,
//                width: 100,
//                targets: [6]
//            }],
//            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
//            language: {
//                search: '<span>Filter:</span> _INPUT_',
//                searchPlaceholder: 'Type to filter...',
//                lengthMenu: '<span>Show:</span> _MENU_',
//                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
//            }
//        });


//        // Basic responsive configuration


//        //$('.datatable-responsive').DataTable();
//        $('#table-productos').DataTable();



//        // Column controlled child rows
//        $('.datatable-responsive-column-controlled').DataTable({
//            responsive: {
//                details: {
//                    type: 'column'
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: 0
//                },
//                {
//                    width: "100px",
//                    targets: [6]
//                },
//                {
//                    orderable: false,
//                    targets: [6]
//                }
//            ],
//            order: [1, 'asc']
//        });


//        // Control position
//        $('.datatable-responsive-control-right').DataTable({
//            responsive: {
//                details: {
//                    type: 'column',
//                    target: -1
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: -1
//                },
//                {
//                    width: "100px",
//                    targets: [5]
//                },
//                {
//                    orderable: false,
//                    targets: [5]
//                }
//            ]
//        });


//        // Whole row as a control
//        $('.datatable-responsive-row-control').DataTable({
//            responsive: {
//                details: {
//                    type: 'column',
//                    target: 'tr'
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: 0
//                },
//                {
//                    width: "100px",
//                    targets: [6]
//                },
//                {
//                    orderable: false,
//                    targets: [6]
//                }
//            ],
//            order: [1, 'asc']
//        });
//    };

//    // Select2 for length menu styling
//    var _componentSelect2 = function () {
//        if (!$().select2) {
//            console.warn('Warning - select2.min.js is not loaded.');
//            return;
//        }

//        // Initialize
//        $('.dataTables_length select').select2({
//            minimumResultsForSearch: Infinity,
//            dropdownAutoWidth: true,
//            width: 'auto'
//        });
//    };


//    //
//    // Return objects assigned to module
//    //

//    return {
//        init: function () {
//            _componentDatatableResponsive();
//            _componentSelect2();
//        }
//    }
//}();
//var DatatableResponsiveCategoriaPro = function () {


//    //
//    // Setup module components
//    //

//    // Basic Datatable examples
//    var _componentDatatableResponsive = function () {
//        if (!$().DataTable) {
//            console.warn('Warning - datatables.min.js is not loaded.');
//            return;
//        }

//        // Setting datatable defaults
//        $.extend($.fn.dataTable.defaults, {
//            autoWidth: false,
//            responsive: true,
//            columnDefs: [{
//                orderable: false,
//                width: 100,
//                targets: [3]
//            }],
//            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
//            language: {
//                search: '<span>Filter:</span> _INPUT_',
//                searchPlaceholder: 'Type to filter...',
//                lengthMenu: '<span>Show:</span> _MENU_',
//                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
//            }
//        });


//        // Basic responsive configuration


//        //$('.datatable-responsive').DataTable();
//        $('#table-catPro').DataTable();



//        // Column controlled child rows
//        $('.datatable-responsive-column-controlled').DataTable({
//            responsive: {
//                details: {
//                    type: 'column'
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: 0
//                },
//                {
//                    width: "100px",
//                    targets: [3]
//                },
//                {
//                    orderable: false,
//                    targets: [3]
//                }
//            ],
//            order: [1, 'asc']
//        });


//        //$('.datatable-responsive-control-right').DataTable({
//        //    responsive: {
//        //        details: {
//        //            type: 'column',
//        //            target: -1
//        //        }
//        //    },
//        //    columnDefs: [
//        //        {
//        //            className: 'control',
//        //            orderable: false,
//        //            targets: -1
//        //        },
//        //        {
//        //            width: "100px",
//        //            targets: [5]
//        //        },
//        //        {
//        //            orderable: false,
//        //            targets: [5]
//        //        }
//        //    ]
//        //});


//        //$('.datatable-responsive-row-control').DataTable({
//        //    responsive: {
//        //        details: {
//        //            type: 'column',
//        //            target: 'tr'
//        //        }
//        //    },
//        //    columnDefs: [
//        //        {
//        //            className: 'control',
//        //            orderable: false,
//        //            targets: 0
//        //        },
//        //        {
//        //            width: "100px",
//        //            targets: [6]
//        //        },
//        //        {
//        //            orderable: false,
//        //            targets: [6]
//        //        }
//        //    ],
//        //    order: [1, 'asc']
//        //});
//    };

//    // Select2 for length menu styling
//    var _componentSelect2 = function () {
//        if (!$().select2) {
//            console.warn('Warning - select2.min.js is not loaded.');
//            return;
//        }

//        // Initialize
//        $('.dataTables_length select').select2({
//            minimumResultsForSearch: Infinity,
//            dropdownAutoWidth: true,
//            width: 'auto'
//        });
//    };


//    //
//    // Return objects assigned to module
//    //

//    return {
//        init: function () {
//            _componentDatatableResponsive();
//            _componentSelect2();
//        }
//    }
//}();
//var DatatableResponsiveEmpresas = function () {


//    //
//    // Setup module components
//    //

//    // Basic Datatable examples
//    var _componentDatatableResponsive = function () {
//        if (!$().DataTable) {
//            console.warn('Warning - datatables.min.js is not loaded.');
//            return;
//        }

//        // Setting datatable defaults
//        $.extend($.fn.dataTable.defaults, {
//            autoWidth: false,
//            responsive: true,
//            columnDefs: [{
//                orderable: false,
//                width: 100,
//                targets: [7]
//            }],
//            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
//            language: {
//                search: '<span>Filter:</span> _INPUT_',
//                searchPlaceholder: 'Type to filter...',
//                lengthMenu: '<span>Show:</span> _MENU_',
//                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
//            }
//        });


//        // Basic responsive configuration


//        //$('.datatable-responsive').DataTable();
//        $('#table-empresas').DataTable();



//        // Column controlled child rows
//        $('.datatable-responsive-column-controlled').DataTable({
//            responsive: {
//                details: {
//                    type: 'column'
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: 0
//                },
//                {
//                    width: "100px",
//                    targets: [7]
//                },
//                {
//                    orderable: false,
//                    targets: [7]
//                }
//            ],
//            order: [1, 'asc']
//        });


//        // Control position
//        $('.datatable-responsive-control-right').DataTable({
//            responsive: {
//                details: {
//                    type: 'column',
//                    target: -1
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: -1
//                },
//                {
//                    width: "100px",
//                    targets: [5]
//                },
//                {
//                    orderable: false,
//                    targets: [5]
//                }
//            ]
//        });


//        // Whole row as a control
//        $('.datatable-responsive-row-control').DataTable({
//            responsive: {
//                details: {
//                    type: 'column',
//                    target: 'tr'
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control',
//                    orderable: false,
//                    targets: 0
//                },
//                {
//                    width: "100px",
//                    targets: [6]
//                },
//                {
//                    orderable: false,
//                    targets: [6]
//                }
//            ],
//            order: [1, 'asc']
//        });
//    };

//    // Select2 for length menu styling
//    var _componentSelect2 = function () {
//        if (!$().select2) {
//            console.warn('Warning - select2.min.js is not loaded.');
//            return;
//        }

//        // Initialize
//        $('.dataTables_length select').select2({
//            minimumResultsForSearch: Infinity,
//            dropdownAutoWidth: true,
//            width: 'auto'
//        });
//    };


//    //
//    // Return objects assigned to module
//    //

//    return {
//        init: function () {
//            _componentDatatableResponsive();
//            _componentSelect2();
//        }
//    }
//}();



// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    DatatableResponsive.init();


});
