﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biomedicina_Admin.Models
{
    public class Producto
    {

        public int Id_producto { get; set; }

        public int Id_empresa { get; set; }

        public int Id_procedencia { get; set; }

        public int Id_marca { get; set; }

        public string Nombre { get; set; }

        public int Id_categoria { get; set; }

        public string Nombre_categoria { get; set; }

        public string Nombre_empresa { get; set; }

        public string Modelo { get; set; }

        public string Descripcion { get; set; }


        public string Capacidad { get; set; }

        public string Temporizador { get; set; }

        public string Temperatura { get; set; }

        public string Dimension_tanque { get; set; }

        public string Potencia { get; set; }

        public string Energia { get; set; }

        public Especificacion_tecnica Espec_tecnica { get; set; }

        //public string Ficha_tecnica { get; set; }

        public HttpPostedFileBase Ficha_tecnica { get; set; }


        public HttpPostedFileBase[] Imagenes { get; set; }

        public string Imagen { get; set; }

    }
}