﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Biomedicina_Admin.Models
{
    public class Empresa
    {
        public int Id_empresa { get; set; }

		[Required(ErrorMessage = "Por favor ingrese una razón social")]
		public string Razon_social { get; set; }

		[Required(ErrorMessage = "Por favor ingrese una razón social")]
		public int Inicio_Op { get; set; }

		[Required(ErrorMessage = "Por favor ingrese el RUC")]
		public string RUC { get; set; }

		[Required(ErrorMessage = "Por favor ingrese una descripción")]
		public string Descripcion { get; set; }

		[Required(ErrorMessage = "Por favor ingrese una dirección")]
		public string Direccion { get; set; }
	
		public DateTime FechaRegistro { get; set; }


	}
}