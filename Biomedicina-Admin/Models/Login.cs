﻿
using System.ComponentModel.DataAnnotations;


namespace Biomedicina_Admin.Models
{
    public class Login
    {

        [Required(ErrorMessage = "Por favor ingrese un nombre de usuario")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Por favor ingrese un password")]
        public string Password { get; set; }
    }
}