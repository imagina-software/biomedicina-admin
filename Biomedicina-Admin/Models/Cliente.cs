﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Biomedicina_Admin.Models
{
    public class Cliente
    {


        public int ClienteId { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre de usuario")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string UserName { get; set; }

        //[Required(ErrorMessage = "Por favor ingrese un password")]
        public string Password { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Por favor ingrese apellido paterno")]
        public string ApPaterno { get; set; }

        [Required(ErrorMessage = "Por favor ingrese apellido materno")]
        public string ApMaterno { get; set; }

        public string Telefono { get; set; }

        public string Email { get; set; }
    }
}