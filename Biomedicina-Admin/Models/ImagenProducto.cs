﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biomedicina_Admin.Models
{
    public class ImagenProducto
    {

        public int Id_imagen { get; set; }
        public int Id_producto { get; set; }
        public string Imagen_url { get; set; }

        public string Descripcion { get; set; }


    }
}