﻿using Biomedicina_Admin.Models;
using Biomedicina_Admin.Repository.Admin;
using System;
using System.Transactions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Biomedicina_Admin.Utility;


namespace Biomedicina_Admin.Controllers
{
    public class MantenimientoController : Controller
    {
        // GET: Mantenimiento
        public ActionResult Index()
        {
            return View();
        }

        #region Mantener Usuario

        public ActionResult MantenerUsuario()
        {

            return View();
        }

        [HttpPost]
        public JsonResult MantenerUsuario(Usuario objUser)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";


            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    MantenimientoAccess objMantenimiento = new MantenimientoAccess();


                    if (objUser.UserId.Equals(0))
                    {
                        accion = "N";
                    }
                    else
                    {
                        accion = "M";
                    }


                    switch (accion)
                    {
                        case "N":
                            objMantenimiento.RegistrarUsuario(objUser);
                            mensaje = "Se grabo correctamente la información.";
                            break;

                        case "M":
                            objMantenimiento.ActualizarUsuario(objUser);
                            mensaje = "Se actualizo correctamente la información.";
                            break;

                        case "E":
                            objMantenimiento.EliminarUsuario(objUser.UserId);
                            mensaje = "Se elimino correctamente la información.";
                            break;
                    }

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";

                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });
        }

        public JsonResult EliminarUsuario(Int16 pNumUsuario = 0)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";

            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                    objMantenimiento.EliminarUsuario(pNumUsuario);
                    mensaje = "Se elimino correctamente la información.";

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";

                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });

        }

        [HttpPost]
        public ActionResult ListarUsuario2(Int16 pNumUsuario = 0)
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            String mensaje = "";
            String resultado = "";
            List<Usuario> lista;

            try
            {
                MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                lista = objMantenimiento.ListarUsuario(pNumUsuario);

                if (pNumUsuario == 0)
                {
                    mensaje = _serializer.Serialize(lista.ToList());
                }
                else
                {

                    mensaje = _serializer.Serialize(lista.Where(col => col.UserId == pNumUsuario).ToList());
                }

                resultado = "OK";
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                resultado = "ER";
            }

            return Json(new { data = mensaje, indicador = resultado }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ListaUsuario3(Int16 pNumUsuario = 0)
        {
            List<Usuario> lista;
            MantenimientoAccess objMantenimiento = new MantenimientoAccess();

            lista = objMantenimiento.ListarUsuario(pNumUsuario);

            return Json(new { data = lista }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ListarUsuario(Int16 pNumUsuario = 0)
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            String mensaje = "";
            String resultado = "";
            List<Usuario> lista;

            try
            {
                MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                lista = objMantenimiento.ListarUsuario(pNumUsuario);

                if (pNumUsuario == 0)
                {
                    string tabla = "<table class='table datatable-responsive'>";

                    tabla += @"<thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Nombre</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Email</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody >";

                    foreach (Usuario item in lista)
                    {
                        tabla += String.Format(@"<tr>
                                                <td>{0}</td>
                                                <td>{1}</td>
                                                <td>{2}</td>
                                                <td>{3}</td>
                                                <td>{4}</td>
                                                <td>{5}</td>
                                                <td class='text-center'>
                                                    <div class='list-icons'>
                                                        <div class='dropdown'>
                                                            <a href='#' class='list-icons-item' data-toggle='dropdown'>
                                                                <i class='icon-menu9'></i>
                                                            </a>
                                                            <div class='dropdown-menu dropdown-menu-right'>
                                                                    <a onclick='editarPasajero({6})' class='dropdown-item'><i class='icon-file-pdf'></i>Editar</a>
                                                                    <a onclick='editarContrasena({7})' class='dropdown-item'><i class='icon-file-pdf'></i>Cambiar Constraseña</a>
                                                                    <a onclick='eliminarPasajero({8})' class='dropdown-item'><i class='icon-file-pdf'></i>Eliminar</a>     
                                                            </div>        
                                                        </div>
                                                    </div>

                                                </td>
                                        
                                             </tr>",
                                               item.UserName, item.Nombre, item.ApPaterno, item.ApMaterno, item.Email, item.IsActive == true ? "Activo" : "Inactivo", item.UserId.ToString(), item.UserId.ToString(), item.UserId.ToString());
                    }

                    tabla += "</tbody></table>";

                    mensaje = tabla;
                }
                else
                {
                    mensaje = _serializer.Serialize(lista.Where(col => col.UserId == pNumUsuario).ToList());
                }

                resultado = "OK";
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                resultado = "ER";
            }

            return Json(new { contenido = mensaje, indicador = resultado });
        }

        [HttpPost]
        public JsonResult ActualizarConstraseña(Int16 pNumUsuario = 0, string pPassword = "")
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";

            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                    objMantenimiento.ActualizarContraseña(pNumUsuario, pPassword);
                    mensaje = "Se actualizo la constraseña.";

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";


                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });

        }

        #endregion

        #region Mantener cliente

        public ActionResult MantenerCliente()
        {

            return View();
        }


        [HttpPost]
        public ActionResult ListarCliente(Int16 pNumCliente = 0)
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            String mensaje = "";
            String resultado = "";
            List<Cliente> lista;

            try
            {
                MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                lista = objMantenimiento.ListarCliente(pNumCliente);

                if (pNumCliente == 0)
                {
                    mensaje = _serializer.Serialize(lista.ToList());
                }
                else
                {

                    mensaje = _serializer.Serialize(lista.Where(col => col.ClienteId == pNumCliente).ToList());
                }

                resultado = "OK";
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                resultado = "ER";
            }

            return Json(new { data = mensaje, indicador = resultado }, JsonRequestBehavior.AllowGet);

        }



        #endregion

        #region Mantener Producto

        public ActionResult MantenerProducto()
        {

            return View();
        }

        [HttpPost]
        public JsonResult MantenerProducto(Producto objProducto)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";
            List<string> url_imagenes;
            BiomedicinaFile objFile = new BiomedicinaFile();
            string idProductoUltimo = string.Empty;



            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    MantenimientoAccess objMantenimiento = new MantenimientoAccess();


                    if (objProducto.Id_producto.Equals(0))
                    {
                        accion = "N";
                    }
                    else
                    {
                        accion = "M";
                    }


                    switch (accion)
                    {
                        case "N":
                            idProductoUltimo=objMantenimiento.RegistrarProducto(objProducto);

                            //url_imagenes = objFile.GuardarImagenes(objProducto.Imagenes);


                            //objMantenimiento.RegistrarUrlImagen(idProductoUltimo, url_imagenes);

                            mensaje = "Se grabo correctamente la información.";
                            break;

                        case "M":
                            objMantenimiento.ActualizarProducto(objProducto);
                            mensaje = "Se actualizo correctamente la información.";
                            break;

                        case "E":
                            objMantenimiento.EliminarProducto(objProducto.Id_producto);
                            mensaje = "Se elimino correctamente la información.";
                            break;
                    }

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";

                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });
        }

        [HttpPost]
        public ActionResult ListarProducto(Int16 pNumProducto = 0)
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            String mensaje = "";
            String mensaje2 = "";
            String resultado = "";
            List<Producto> lista;
            List<ImagenProducto> lista_url;


            try
            {
                MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                lista = objMantenimiento.ListarProducto(pNumProducto);
                lista_url = objMantenimiento.ListarImagenUrl(pNumProducto);

                if (pNumProducto == 0)
                {
                    mensaje = _serializer.Serialize(lista.ToList());
                    mensaje2= _serializer.Serialize(lista_url.ToList());
                }
                else
                {

                    mensaje = _serializer.Serialize(lista.Where(col => col.Id_producto == pNumProducto).ToList());
                    mensaje2 = _serializer.Serialize(lista_url.Where(col => col.Id_producto == pNumProducto).ToList());

                }

                resultado = "OK";
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                resultado = "ER";
            }

            return Json(new { data = mensaje,data2=mensaje2, indicador = resultado }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult EliminarProducto(Int16 pNumProducto = 0)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";

            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                    objMantenimiento.EliminarProducto(pNumProducto);
                    mensaje = "Se elimino correctamente la información.";

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";

                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });

        }

        public ActionResult ObtenerCategoriaProducto()
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            String mensaje = "";
            String resultado = "";

            List<CategoriaProducto> lista = null;


            try
            {

                MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                lista = objMantenimiento.ListarCategoriaProducto(0);


                //MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                //lista = objMantenimiento.ListarTipoServicio();


                //mensaje = _serializer.Serialize(lista.ToList());


                resultado = "OK";

            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                resultado = "ER";

            }
            return Json(new { data = lista.ToList(), indicador = resultado }, JsonRequestBehavior.AllowGet);

            //return Json(lista.ToList(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult ObtenerEmpresa()
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            String mensaje = "";
            String resultado = "";

            List<Empresa> lista = null;


            try
            {

                MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                lista = objMantenimiento.ListarEmpresa();


                //MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                //lista = objMantenimiento.ListarTipoServicio();


                //mensaje = _serializer.Serialize(lista.ToList());


                resultado = "OK";

            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                resultado = "ER";

            }
            return Json(new { data = lista.ToList(), indicador = resultado }, JsonRequestBehavior.AllowGet);

            //return Json(lista.ToList(), JsonRequestBehavior.AllowGet);

        }
		#endregion
		
		#region Mantener servicio

		//public ActionResult MantenerServicio()
		//{

		//    return View();
		//}

		//[HttpPost]

		//public JsonResult MantenerServicio(Servicio objServicio)
		//{
		//    String mensaje = "";
		//    String resultado = "";
		//    String valid = "";
		//    String fecha = "";
		//    String accion = "";


		//    using (TransactionScope transaccion = new TransactionScope())
		//    {

		//        try
		//        {
		//            MantenimientoAccess objMantenimiento = new MantenimientoAccess();


		//            objMantenimiento.RegistrarServicio(objServicio);
		//            mensaje = "Se grabo correctamente la información.";


		//            transaccion.Complete();

		//            resultado = "OK";

		//        }
		//        catch (Exception ex)
		//        {
		//            transaccion.Dispose();

		//            mensaje = ex.Message;
		//            resultado = "ER";


		//        }

		//    }
		//    return Json(new { contenido = mensaje, indicador = resultado });






		//}


		//public ActionResult ObtenerTipoServicio()
		//{
		//    JavaScriptSerializer _serializer = new JavaScriptSerializer();
		//    String mensaje = "";
		//    String resultado = "";

		//    List<TipoServicio> lista = null;


		//    try
		//    {

		//        MantenimientoAccess objMantenimiento = new MantenimientoAccess();

		//        lista = objMantenimiento.ListarTipoServicio();


		//        //MantenimientoAccess objMantenimiento = new MantenimientoAccess();

		//        //lista = objMantenimiento.ListarTipoServicio();


		//        //mensaje = _serializer.Serialize(lista.ToList());


		//        resultado = "OK";

		//    }
		//    catch (Exception ex)
		//    {
		//        mensaje = ex.Message;
		//        resultado = "ER";

		//    }
		//    return Json(new { data = lista.ToList(), indicador = resultado }, JsonRequestBehavior.AllowGet);

		//    //return Json(lista.ToList(), JsonRequestBehavior.AllowGet);

		//}
		#endregion

		#region Mantener Empresa

		public ActionResult MantenerEmpresa()
		{

			return View();
		}


		[HttpPost]
		public JsonResult MantenerEmpresa(Empresa objEmpresa)
		{

			String mensaje = "";
			String resultado = "";
			String valid = "";
			String fecha = "";
			String accion = "";


			using (TransactionScope transaccion = new TransactionScope())
			{

				try
				{
					MantenimientoAccess objMantenimiento = new MantenimientoAccess();


					if (objEmpresa.Id_empresa.Equals(0))
					{
						accion = "N";
					}
					else
					{
						accion = "M";
					}


					switch (accion)
					{
						case "N":
							objMantenimiento.RegistrarEmpresa(objEmpresa);
							mensaje = "Se grabo correctamente la información.";
							break;

						case "M":
							objMantenimiento.ActualizarEmpresa(objEmpresa);
							mensaje = "Se actualizo correctamente la información.";
							break;

						case "E":
							objMantenimiento.EliminarEmpresa(objEmpresa.Id_empresa);
							mensaje = "Se elimino correctamente la información.";
							break;
					}

					transaccion.Complete();

					resultado = "OK";

				}
				catch (Exception ex)
				{
					transaccion.Dispose();

					mensaje = ex.Message;
					resultado = "ER";

				}

			}
			return Json(new { contenido = mensaje, indicador = resultado });
		}



		public JsonResult EliminarEmpresa(Int16 pNumEmpresa = 0)
		{

			String mensaje = "";
			String resultado = "";
			String valid = "";
			String fecha = "";
			String accion = "";

			using (TransactionScope transaccion = new TransactionScope())
			{

				try
				{
					MantenimientoAccess objMantenimiento = new MantenimientoAccess();

					objMantenimiento.EliminarEmpresa(pNumEmpresa);
					mensaje = "Se inactivo correctamente la empresa.";

					transaccion.Complete();

					resultado = "OK";

				}
				catch (Exception ex)
				{
					transaccion.Dispose();

					mensaje = ex.Message;
					resultado = "ER";

				}

			}
			return Json(new { contenido = mensaje, indicador = resultado });

		}





		[HttpPost]
		public ActionResult ListarEmpresaAdm(Int16 pNumEmpresa = 0)
		{
			JavaScriptSerializer _serializer = new JavaScriptSerializer();
			String mensaje = "";
			String resultado = "";
			List<Empresa> lista;

			try
			{
				MantenimientoAccess objMantenimiento = new MantenimientoAccess();

				lista = objMantenimiento.ListarEmpresa(pNumEmpresa);

				if (pNumEmpresa == 0)
				{
					mensaje = _serializer.Serialize(lista.ToList());
				}
				else
				{

					mensaje = _serializer.Serialize(lista.Where(col => col.Id_empresa == pNumEmpresa).ToList());
				}

				resultado = "OK";
			}
			catch (Exception ex)
			{
				mensaje = ex.Message;
				resultado = "ER";
			}

			return Json(new { data = mensaje, indicador = resultado }, JsonRequestBehavior.AllowGet);

		}





		#endregion

		#region Mantener Categoria Producto

		public ActionResult MantenerCatPro()
		{

			return View();
		}


		[HttpPost]
		public JsonResult MantenerCatPro(CategoriaProducto objCatPro)
		{

			String mensaje = "";
			String resultado = "";
			String valid = "";
			String fecha = "";
			String accion = "";


			using (TransactionScope transaccion = new TransactionScope())
			{

				try
				{
					MantenimientoAccess objMantenimiento = new MantenimientoAccess();


					if (objCatPro.Id_categoria.Equals(0))
					{
						accion = "N";
					}
					else
					{
						accion = "M";
					}


					switch (accion)
					{
						case "N":
							objMantenimiento.RegistrarCategoriaPro(objCatPro);
							mensaje = "Se grabo correctamente la información.";
							break;

						case "M":
							objMantenimiento.ActualizarCategoriaPro(objCatPro);
							mensaje = "Se actualizo correctamente la información.";
							break;

						case "E":
							objMantenimiento.EliminarCategoriaPro(objCatPro.Id_categoria);
							mensaje = "Se elimino correctamente la información.";
							break;
					}

					transaccion.Complete();

					resultado = "OK";

				}
				catch (Exception ex)
				{
					transaccion.Dispose();

					mensaje = ex.Message;
					resultado = "ER";

				}

			}
			return Json(new { contenido = mensaje, indicador = resultado });
		}



		[HttpPost]
		public ActionResult ListarCategoriaPro(Int16 pNumCategoria = 0)
		{
			JavaScriptSerializer _serializer = new JavaScriptSerializer();
			String mensaje = "";
			String resultado = "";
			List<CategoriaProducto> lista;

			try
			{
				MantenimientoAccess objMantenimiento = new MantenimientoAccess();

				lista = objMantenimiento.ListarCategoriaProducto(pNumCategoria);

				if (pNumCategoria == 0)
				{
					mensaje = _serializer.Serialize(lista.ToList());
				}
				else
				{

					mensaje = _serializer.Serialize(lista.Where(col => col.Id_categoria == pNumCategoria).ToList());
				}

				resultado = "OK";
			}
			catch (Exception ex)
			{
				mensaje = ex.Message;
				resultado = "ER";
			}

			return Json(new { data = mensaje, indicador = resultado }, JsonRequestBehavior.AllowGet);

		}





		#endregion



	}
}