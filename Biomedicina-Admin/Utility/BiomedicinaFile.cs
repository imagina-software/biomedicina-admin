﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace Biomedicina_Admin.Utility
{
    public class BiomedicinaFile
    {
        const string strSecureAppSettings = "secureAppSettings";

        public string GuardarFile(HttpPostedFileBase ficha_tecnica) {

            try
            {

                string url_file = string.Empty;

                //Use Namespace called :  System.IO
                string FileName = Path.GetFileNameWithoutExtension(ficha_tecnica.FileName);

                //To Get File Extension
                string FileExtension = Path.GetExtension(ficha_tecnica.FileName);

                //Add Current Date To Attached File Name
                FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + FileName.Trim() + FileExtension;

                //Get Upload path from Web.Config file AppSettings.
                //string UploadPath = ((NameValueCollection)WebConfigurationManager.GetSection(strSecureAppSettings))[("UserFilePath")].ToString();

                //string UploadPath2 = System.Web.HttpContext.Current.Server.MapPath("~/App_Data");

                string UploadPath3 = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Pdf"), Path.GetFileName(ficha_tecnica.FileName));


                //Its Create complete path to store in server.
                url_file = UploadPath3 + FileName;

                //To copy and save file into server.

                ficha_tecnica.SaveAs(UploadPath3);
                //membervalues.ImageFile.SaveAs(membervalues.ImagePath);

                return url_file;
            }
            catch (Exception ex) {

                throw new Exception { Source = ex.ToString() };

            }


        }

        public List<string> GuardarImagenes(HttpPostedFileBase[] imagenes)
        {
            string NombreImagen = string.Empty;
            string NombreExtension = string.Empty;
            string UploadPathGG = string.Empty;
            string url_imagen = string.Empty;
            List<string> lst_url_imagenes = new List<string>();

            try
            {

                foreach (HttpPostedFileBase imagen in imagenes) {

                    if(imagen != null){

                        //Use Namespace called :  System.IO
                        NombreImagen = Path.GetFileNameWithoutExtension(imagen.FileName);

                        //To Get File Extension
                        NombreExtension = Path.GetExtension(imagen.FileName);

                        //Add Current Date To Attached File Name
                        NombreImagen = DateTime.Now.ToString("yyyyMMdd") + "-" + NombreImagen.Trim() + NombreExtension;

                        //Get Upload path from Web.Config file AppSettings.
                        //string UploadPath = ((NameValueCollection)WebConfigurationManager.GetSection(strSecureAppSettings))[("UserFilePath")].ToString();

                        //string UploadPath2 = System.Web.HttpContext.Current.Server.MapPath("~/App_Data");

                        UploadPathGG = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Imagenes"), Path.GetFileName(imagen.FileName));


                        //Its Create complete path to store in server.
                        url_imagen = UploadPathGG + NombreImagen;

                        //To copy and save file into server.

                        imagen.SaveAs(UploadPathGG);

                        lst_url_imagenes.Add(url_imagen);

                    }

                }

                return lst_url_imagenes;
            }
            catch (Exception ex)
            {

                throw new Exception { Source = ex.ToString() };

            }

        }
    }
}