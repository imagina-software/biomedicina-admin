﻿using Biomedicina_Admin.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Biomedicina_Admin.Repository.Admin
{
    public class LoginAccess
    {

        public IEnumerable<Usuario> Login(string pUserName, string pPassword)
        {

            string lineagg = "0";

            try
            {

                List<Usuario> lstUserProfile = new List<Usuario>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("dbo.BIO_LeeUsuarioPassword_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pUserName", SqlDbType.VarChar).Value = pUserName;
                    cmd.Parameters.Add("@pPassword", SqlDbType.VarChar).Value = pPassword;

                    lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    lineagg += ",3";
                    while (rdr.Read())
                    {
                        lineagg += ",4";
                        if (rdr["usuario"].ToString().Trim() == pUserName.Trim())
                        {

                            Usuario fuserprofile = new Usuario
                            {
                                UserId = Convert.ToInt32(rdr["id_usuario"]),
                                UserName = rdr["usuario"].ToString(),
                                //IsActive = Convert.ToInt32(rdr["IsActive"]),
                                Nombre = rdr["nombre"].ToString(),
                                ApPaterno = rdr["ap_paterno"].ToString(),
                                ApMaterno = rdr["ap_materno"].ToString()
                                //Email = rdr["Email"].ToString()
                            };

                            lstUserProfile.Add(item: fuserprofile);

                        }

                    }
                    lineagg += ",5";
                    con.Close();
                }

                return lstUserProfile;

            }
            catch (Exception ex)
            {

                //throw;
                //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                throw new Exception { Source = ex.ToString() };

            }

        }
    }
}