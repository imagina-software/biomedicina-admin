﻿using Biomedicina_Admin.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Biomedicina_Admin.Utility;

namespace Biomedicina_Admin.Repository.Admin
{
    public class MantenimientoAccess
    {
        #region Mantenimiento de Usuario

            public void RegistrarUsuario(Usuario objUser)
            {
                string codigo = "";
                //char activo;

                //if (objUser.IsActive)
                //{
                //    activo = 'S';
                //}
                //else
                //{
                //    activo = 'N';
                //}



                try
                {

                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {

                        SqlCommand cmd = new SqlCommand("dbo.BIO_RegistrarUsuario_I", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@pNroUsuario", SqlDbType.Int).Value = objUser.UserId;
                        cmd.Parameters.Add("@pUserName", SqlDbType.VarChar).Value = objUser.UserName;
                        cmd.Parameters.Add("@pPassword", SqlDbType.VarChar).Value = objUser.Password;
                        cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objUser.Nombre;
                        cmd.Parameters.Add("@pApPaterno", SqlDbType.VarChar).Value = objUser.ApPaterno;
                        cmd.Parameters.Add("@pApMaterno", SqlDbType.VarChar).Value = objUser.ApMaterno;
                        cmd.Parameters.Add("@pEmail", SqlDbType.VarChar).Value = objUser.Email;
                        cmd.Parameters.Add("@pActivo", SqlDbType.Bit).Value = objUser.IsActive;

                        con.Open();
                        codigo = (String)cmd.ExecuteScalar();

                        con.Close();
                    }


                }
                catch (Exception ex)
                {

                    //throw;
                    //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                    //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                    throw new Exception { Source = ex.ToString() };

                }

            }

            public void ActualizarUsuario(Usuario objUser)
            {
                string codigo = "";

                try
                {

                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {

                        SqlCommand cmd = new SqlCommand("dbo.BIO_ActualizarUsuario_U", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@pNroUsuario", SqlDbType.Int).Value = objUser.UserId;
                        cmd.Parameters.Add("@pUserName", SqlDbType.VarChar).Value = objUser.UserName;
                        cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objUser.Nombre;
                        cmd.Parameters.Add("@pApPaterno", SqlDbType.VarChar).Value = objUser.ApPaterno;
                        cmd.Parameters.Add("@pApMaterno", SqlDbType.VarChar).Value = objUser.ApMaterno;
                        cmd.Parameters.Add("@pEmail", SqlDbType.VarChar).Value = objUser.Email;
                        cmd.Parameters.Add("@pActivo", SqlDbType.Bit).Value = objUser.IsActive;



                        con.Open();
                        codigo = (String)cmd.ExecuteScalar();

                        con.Close();
                    }


                }
                catch (Exception ex)
                {

                    //throw;
                    //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                    //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                    throw new Exception { Source = ex.ToString() };

                }

            }

            public void EliminarUsuario(int pUserId)
            {

                try
                {

                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {
                        SqlCommand cmd = new SqlCommand("dbo.BIO_EliminarUsuario_D", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@UserId", pUserId);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
                catch (Exception ex)
                {

                    throw;
                }


            }

            public void ActualizarContraseña(int pUserId, string pPassword)
            {

                try
                {

                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {
                        SqlCommand cmd = new SqlCommand("dbo.BIO_ActualizarContraseña_U", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@pUserId", pUserId);
                        cmd.Parameters.AddWithValue("@pContraseña", pPassword);


                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
                catch (Exception ex)
                {

                    throw;
                }


            }

            public List<Usuario> ListarUsuario(int pUserId)
            {
                var lista = new List<Usuario>();
                var dt = new DataTable();

                try
                {
                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {
                        SqlCommand cmd = new SqlCommand("dbo.BIO_ListarUsuario_L", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@UserId", pUserId);

                        con.Open();
                        dt.Load(cmd.ExecuteReader());
                        con.Close();

                        lista = dt.AsEnumerable().Select(x => new Usuario
                        {
                            UserId = x.Field<Int16>("id_usuario"),
                            UserName = x.Field<string>("usuario"),
                            Nombre = x.Field<string>("nombre"),
                            ApPaterno = x.Field<string>("ap_paterno"),
                            ApMaterno = x.Field<string>("ap_materno"),
                            Email = x.Field<string>("email"),
                            IsActive = x.Field<bool>("estado")

                        })
                        .ToList();
                    }
                }
                catch (Exception ex)
                {
                    lista = new List<Usuario>();
                }

                return lista;
            }

        #endregion

        #region Mantenimiento clientes
        public List<Cliente> ListarCliente(int pClienteId)
        {
            var lista = new List<Cliente>();
            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.BIO_ListarCliente_L", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", pClienteId);

                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new Cliente
                    {
                        ClienteId = x.Field<Int16>("id_cliente"),
                        UserName = x.Field<string>("usuario"),
                        Nombre = x.Field<string>("nombre"),
                        ApPaterno = x.Field<string>("apellido_paterno"),
                        ApMaterno = x.Field<string>("apellido_materno"),
                        Email = x.Field<string>("correo"),
                        IsActive = x.Field<bool>("estado")

                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                lista = new List<Cliente>();
            }

            return lista;
        }

        #endregion

        #region Mantenimiento producto

            public string RegistrarProducto(Producto objProducto)
            {
                string codigo = "";
                string mensaje = string.Empty;
                string url_ficha_tecnica = string.Empty;
                string idProductoUltimo =string.Empty;
                List<string> url_imagenes;
                BiomedicinaFile objFile = new BiomedicinaFile();
                int cont = 0;
           
                url_ficha_tecnica = objFile.GuardarFile(objProducto.Ficha_tecnica);
                url_imagenes = objFile.GuardarImagenes(objProducto.Imagenes);

                var dtUrl = new DataTable();
                dtUrl.Columns.Add("id", typeof(int));
                dtUrl.Columns.Add("imagen_url", typeof(string));

                foreach (string url in url_imagenes)
                {
                    cont = cont + 1;
                    dtUrl.Rows.Add(cont, url);
                }

                try
                {

                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {

                        SqlCommand cmd = new SqlCommand("dbo.BIO_RegistrarProducto_I", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        //cmd.Parameters.Add("@pIdProducto", SqlDbType.Int).Value = objProducto.Id_producto;
                        cmd.Parameters.Add("@pIdEmpresa", SqlDbType.Int).Value = objProducto.Id_empresa;
                        cmd.Parameters.Add("@pIdCatPro", SqlDbType.Int).Value = objProducto.Id_categoria;
                        //cmd.Parameters.Add("@pIdProcedencia", SqlDbType.Int).Value = objProducto.Id_procedencia;
                        //cmd.Parameters.Add("@pIdMarca", SqlDbType.Int).Value = objProducto.Id_marca;
                        cmd.Parameters.Add("@pDescripcion", SqlDbType.VarChar).Value = objProducto.Descripcion;
                        cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objProducto.Nombre;
                        cmd.Parameters.Add("@pUrlFichaTecnica", SqlDbType.VarChar).Value = url_ficha_tecnica;
                        //cmd.Parameters.Add("@pImagen", SqlDbType.VarChar).Value = objProducto.Imagen;
                        cmd.Parameters.Add("@pModelo ", SqlDbType.VarChar).Value = objProducto.Modelo;

                        cmd.Parameters.Add("@pCapacidad", SqlDbType.VarChar).Value = objProducto.Capacidad;
                        cmd.Parameters.Add("@pTemporizador", SqlDbType.VarChar).Value = objProducto.Temporizador;
                        cmd.Parameters.Add("@pTemperatura", SqlDbType.VarChar).Value = objProducto.Temperatura;
                        cmd.Parameters.Add("@pDimension", SqlDbType.VarChar).Value = objProducto.Dimension_tanque;
                        cmd.Parameters.Add("@pPotencia", SqlDbType.VarChar).Value = objProducto.Potencia;
                        cmd.Parameters.Add("@pEnergia", SqlDbType.VarChar).Value = objProducto.Energia;

                        cmd.Parameters.Add("@pLstUrl", SqlDbType.Structured).Value = dtUrl;
                        cmd.Parameters["@pLstUrl"].TypeName = "TypeImagenesUrl";

                        cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@pIdProductoUltimo", SqlDbType.Int).Direction = ParameterDirection.Output;

                        con.Open();
                        codigo = (String)cmd.ExecuteScalar();

                        mensaje = cmd.Parameters["@MsgTrans"].Value.ToString();
                        idProductoUltimo = cmd.Parameters["@pIdProductoUltimo"].Value.ToString();

                     //gg =Convert.ToInt32( cmd.Parameters["@pIdProductoUltimo"].Value.ToString() );

                        con.Close();
                    }

                    return idProductoUltimo;
                //RegistrarUrlImagen(idProductoUltimo, url_imagenes);

                }
                catch (Exception ex)
                {

                    //throw;
                    //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                    //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                    throw new Exception { Source = ex.ToString() };

                }

            }

            public void RegistrarUrlImagen(string idProducto,List<string> list_url_imagen) {

                string codigo = "";
                string mensaje = string.Empty;


            try
            {

                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {


                        foreach (string url in list_url_imagen) {

                            SqlCommand cmd = new SqlCommand("dbo.BIO_RegistrarUrlImagen_I", con);

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@pIdProducto", SqlDbType.SmallInt).Value =Convert.ToInt16(idProducto);
                            cmd.Parameters.Add("@pImagenUrl", SqlDbType.Int).Value = url;
                            //cmd.Parameters.Add("@pIdProcedencia", SqlDbType.Int).Value = objProducto.Id_procedencia;
                            //cmd.Parameters.Add("@pIdMarca", SqlDbType.Int).Value = objProducto.Id_marca;


                            cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;



                            con.Open();
                            codigo = (String)cmd.ExecuteScalar();

                            mensaje = cmd.Parameters["@MsgTrans"].Value.ToString();


                            con.Close();


                        }


                   
                    }



                }
                catch (Exception ex)
                {

                    //throw;
                    //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                    //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                    throw new Exception { Source = ex.ToString() };

                }


            }
            public List<Producto> ListarProducto(int pProductoId)
            {
                var lista = new List<Producto>();
                var listaEspec = new List<Especificacion_tecnica>();

            var dt = new DataTable();

                try
                {
                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {
                        SqlCommand cmd = new SqlCommand("dbo.BIO_ListarProducto2_L", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@pIdProducto", pProductoId);

                        con.Open();
                        dt.Load(cmd.ExecuteReader());
                        con.Close();

                        lista = dt.AsEnumerable().Select(x => new Producto
                        {
                            Id_producto = x.Field<Int16>("id_producto"),
                            Nombre = x.Field<string>("nombre_producto"),
                            Nombre_empresa = x.Field<string>("nombre_empresa"),
                            Nombre_categoria = x.Field<string>("nombre_categoria"),
                            Id_empresa = x.Field<Int16>("id_producto"),
                            Id_categoria = x.Field<Int16>("id_categoria_pro"),
                            Descripcion = x.Field<string>("descripcion"),
                            Modelo = x.Field<string>("modelo"),
                            Capacidad = x.Field<string>("capacidad"),
                            Temporizador = x.Field<string>("temporizador"),
                            Temperatura = x.Field<string>("temperatura"),
                            Dimension_tanque = x.Field<string>("dimension"),
                            Potencia = x.Field<string>("potencia"),
                            Energia = x.Field<string>("energia")


                        })
                        .ToList();

                
                    }
                }
                catch (Exception ex)
                {
                    lista = new List<Producto>();
                }

                return lista;
            }

            public List<ImagenProducto> ListarImagenUrl(int pProductoId)
            {
            var lista = new List<ImagenProducto>();
            var listaEspec = new List<Especificacion_tecnica>();

            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.BIO_ListarImagenProducto_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@pIdProducto", pProductoId);

                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new ImagenProducto
                    {
                        Id_producto = x.Field<Int16>("id_producto"),
                        Id_imagen = x.Field<Int16>("id_imagen"),
                        Imagen_url = x.Field<string>("imagen_url"),
                        Descripcion = x.Field<string>("descripcion")


                    })
                    .ToList();


                }
            }
            catch (Exception ex)
            {
                lista = new List<ImagenProducto>();
            }

            return lista;
            }
            public void ActualizarProducto(Producto objProducto)
            {
                string codigo = "";
                string mensaje = string.Empty;
                string url_ficha_tecnica = string.Empty;
                string idProductoUltimo = string.Empty;
                List<string> url_imagenes;
                BiomedicinaFile objFile = new BiomedicinaFile();
                int cont = 0;

                url_ficha_tecnica = objFile.GuardarFile(objProducto.Ficha_tecnica);
                url_imagenes = objFile.GuardarImagenes(objProducto.Imagenes);

                var dtUrl = new DataTable();
                dtUrl.Columns.Add("id", typeof(int));
                dtUrl.Columns.Add("id_producto", typeof(int));
                dtUrl.Columns.Add("imagen_url", typeof(string));

                foreach (string url in url_imagenes)
                {
                    cont = cont + 1;
                    dtUrl.Rows.Add(cont, objProducto.Id_producto,url);
                }

            try
                {

                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {

                        SqlCommand cmd = new SqlCommand("dbo.BIO_ActualizarProducto_U", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@pIdProducto", SqlDbType.Int).Value = objProducto.Id_producto;
                        cmd.Parameters.Add("@pIdEmpresa", SqlDbType.Int).Value = objProducto.Id_empresa;
                        cmd.Parameters.Add("@pIdCatPro", SqlDbType.Int).Value = objProducto.Id_categoria;

                    //cmd.Parameters.Add("@pIdProcedencia", SqlDbType.Int).Value = objProducto.Id_procedencia;
                    //cmd.Parameters.Add("@pIdMarca", SqlDbType.Int).Value = objProducto.Id_marca;
                        cmd.Parameters.Add("@pDescripcion", SqlDbType.VarChar).Value = objProducto.Descripcion;
                        cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objProducto.Nombre;
                        cmd.Parameters.Add("@pUrlFichaTecnica", SqlDbType.VarChar).Value = url_ficha_tecnica;
                        cmd.Parameters.Add("@pModelo ", SqlDbType.VarChar).Value = objProducto.Modelo;


                        cmd.Parameters.Add("@pCapacidad", SqlDbType.VarChar).Value = objProducto.Capacidad;
                        cmd.Parameters.Add("@pTemporizador", SqlDbType.VarChar).Value = objProducto.Temporizador;
                        cmd.Parameters.Add("@pTemperatura", SqlDbType.VarChar).Value = objProducto.Temperatura;
                        cmd.Parameters.Add("@pDimension", SqlDbType.VarChar).Value = objProducto.Dimension_tanque;
                        cmd.Parameters.Add("@pPotencia", SqlDbType.VarChar).Value = objProducto.Potencia;
                        cmd.Parameters.Add("@pEnergia", SqlDbType.VarChar).Value = objProducto.Energia;

                        cmd.Parameters.Add("@pLstUrl", SqlDbType.Structured).Value = dtUrl;
                        cmd.Parameters["@pLstUrl"].TypeName = "TypeImagenesUrlUpdate";


                        cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;




                        con.Open();
                        codigo = (String)cmd.ExecuteScalar();

                        mensaje = cmd.Parameters["@MsgTrans"].Value.ToString();

                        con.Close();
                    }


                }
                catch (Exception ex)
                {

                    //throw;
                    //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                    //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                    throw new Exception { Source = ex.ToString() };

                }

            }
            public void EliminarProducto(int pProductoId)
            {

                try
                {

                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {
                        SqlCommand cmd = new SqlCommand("dbo.BIO_EliminarProducto_D", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@pProductoId", pProductoId);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
                catch (Exception ex)
                {

                    throw;
                }


            }

            public List<CategoriaProducto> ListarCategoriaProducto(int pCategoriaId)
            {
                var lista = new List<CategoriaProducto>();
                var dt = new DataTable();

                try
                {
                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {
                        SqlCommand cmd = new SqlCommand("dbo.BIO_ListarCategoriaProducto_L", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@pIdCategoria", pCategoriaId);

                    con.Open();
                        dt.Load(cmd.ExecuteReader());
                        con.Close();

                        lista = dt.AsEnumerable().Select(x => new CategoriaProducto
                        {
                            Id_categoria = x.Field<Int16>("id_categoria_pro"),
                            Nombre_categoria = x.Field<string>("nombre")

                        })
                        .ToList();
                    }
                }
                catch (Exception ex)
                {
                    lista = new List<CategoriaProducto>();
                }

                return lista;
            }

            public List<Empresa> ListarEmpresa()
            {
                var lista = new List<Empresa>();
                var dt = new DataTable();

                try
                {
                    using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                    {
                        SqlCommand cmd = new SqlCommand("dbo.BIO_ListarEmpresa_L", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        //cmd.Parameters.AddWithValue("@pCategoria", objProducto.Id_categoria);

                        con.Open();
                        dt.Load(cmd.ExecuteReader());
                        con.Close();

                        lista = dt.AsEnumerable().Select(x => new Empresa
                        {
                            Id_empresa = x.Field<short>("id_empresa"),
                            Razon_social = x.Field<string>("razon_social")

                        })
                        .ToList();
                    }
                }
                catch (Exception ex)
                {
                    lista = new List<Empresa>();
                }

                return lista;
            }

		#endregion		

		#region Mantenimiento de Empresa

		public void RegistrarEmpresa(Empresa objEmpresa)
		{
			string codigo = "";
			
			try
			{

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("dbo.BIO_RegistrarEmpresa_I", con);

					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar).Value = ParameterDirection.Output;

					cmd.Parameters.Add("@pIdEmpresa", SqlDbType.Int).Value = objEmpresa.Id_empresa;
					cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objEmpresa.Razon_social;
					cmd.Parameters.Add("@pDireccion", SqlDbType.VarChar).Value = objEmpresa.Direccion;
					cmd.Parameters.Add("@pInicio", SqlDbType.Int).Value = objEmpresa.Inicio_Op;
					cmd.Parameters.Add("@pRUC", SqlDbType.VarChar).Value = objEmpresa.RUC;
					cmd.Parameters.Add("@pDescripcion", SqlDbType.VarChar).Value = objEmpresa.Descripcion;


					con.Open();
					codigo = (String)cmd.ExecuteScalar();

					con.Close();
				}


			}
			catch (Exception ex)
			{


				throw new Exception { Source = ex.ToString() };

			}

		}

		public void ActualizarEmpresa(Empresa objEmpresa)
		{
			string codigo = "";

			try
			{

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("dbo.BIO_ActualizarEmpresa_U", con);

					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.Add("@pIdEmpresa", SqlDbType.Int).Value = objEmpresa.Id_empresa;
					cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objEmpresa.Razon_social;
					cmd.Parameters.Add("@pDireccion", SqlDbType.VarChar).Value = objEmpresa.Direccion;
					cmd.Parameters.Add("@pInicio", SqlDbType.VarChar).Value = objEmpresa.Inicio_Op;
					cmd.Parameters.Add("@pRUC", SqlDbType.VarChar).Value = objEmpresa.RUC;
					cmd.Parameters.Add("@pDescripcion", SqlDbType.VarChar).Value = objEmpresa.Descripcion;
	

					con.Open();
					codigo = (String)cmd.ExecuteScalar();

					con.Close();
				}


			}
			catch (Exception ex)
			{

				throw new Exception { Source = ex.ToString() };

			}

			}

			public void EliminarEmpresa(int pEmpresaId)
		{

			try
			{

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand("dbo.BIO_EliminarEmpresa_D", con);

					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@pIdEmpresa", pEmpresaId);

					con.Open();
					cmd.ExecuteNonQuery();
					con.Close();
				}

			}
			catch (Exception ex)
			{

				throw;
			}
		}

			public List<Empresa> ListarEmpresa(int pIdEmpresa)
		{
			var lista = new List<Empresa>();
			var dt = new DataTable();

			try
			{
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand("dbo.BIO_ListarEmpresaADM_L", con);

					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@IdEmpresa", pIdEmpresa);

					con.Open();
					dt.Load(cmd.ExecuteReader());
					con.Close();

					lista = dt.AsEnumerable().Select(x => new Empresa
					{
						Id_empresa = x.Field<Int16>("id_empresa"),
						Razon_social = x.Field<string>("razon_social"),
						Direccion = x.Field<string>("direccion"),
						Inicio_Op = x.Field<int>("inicio_operacion_year"),
						RUC = x.Field<string>("ruc"),
						Descripcion = x.Field<string>("descripcion")

					})
					.ToList();
				}
			}
			catch (Exception ex)
			{
				lista = new List<Empresa>();
			}

			return lista;
		}

		#endregion



		#region Mantenimiento de Categoria Producto

		public void RegistrarCategoriaPro(CategoriaProducto objCatPro)
		{
			string codigo = "";

			try
			{

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("dbo.BIO_RegistrarCategoriaPro_I", con);

					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar).Value = ParameterDirection.Output;

					cmd.Parameters.Add("@pIdCat", SqlDbType.Int).Value = objCatPro.Id_categoria;
					cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objCatPro.Nombre_categoria;


					con.Open();
					codigo = (String)cmd.ExecuteScalar();

					con.Close();
				}


			}
			catch (Exception ex)
			{


				throw new Exception { Source = ex.ToString() };

			}

		}

		public void ActualizarCategoriaPro(CategoriaProducto objEmpresa)
		{
			string codigo = "";

			try
			{

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("dbo.BIO_ActualizarCatPro_U", con);

					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.Add("@pIdCat", SqlDbType.Int).Value = objEmpresa.Id_categoria;
					cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objEmpresa.Nombre_categoria;


					con.Open();
					codigo = (String)cmd.ExecuteScalar();

					con.Close();
				}


			}
			catch (Exception ex)
			{

				throw new Exception { Source = ex.ToString() };

			}

		}

		public void EliminarCategoriaPro(int pIdCar)
		{

			try
			{

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand("dbo.BIO_EliminarEmpresa_D", con);

					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@pIdEmpresa", pIdCar);

					con.Open();
					cmd.ExecuteNonQuery();
					con.Close();
				}

			}
			catch (Exception ex)
			{

				throw;
			}
		}

		public List<CategoriaProducto> ListarCategoriaPro(int pIdCat)
		{
			var lista = new List<CategoriaProducto>();
			var dt = new DataTable();

			try
			{
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand("dbo.BIO_ListarCategoriaProducto_L", con);

					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@IdEmpresa", pIdCat);

					con.Open();
					dt.Load(cmd.ExecuteReader());
					con.Close();

					lista = dt.AsEnumerable().Select(x => new CategoriaProducto
					{
						Id_categoria = x.Field<Int16>("id_categoria_pro"),
						Nombre_categoria = x.Field<string>("nombre")

					})
					.ToList();
				}
			}
			catch (Exception ex)
			{
				lista = new List<CategoriaProducto>();
			}

			return lista;
		}

		#endregion



	}
}