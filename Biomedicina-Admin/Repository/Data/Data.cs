﻿using System.Collections.Specialized;
using System.Web.Configuration;


namespace Biomedicina_Admin.Repository.Data
{
    public class Data
    {


        const string strSecureAppSettings = "secureAppSettings";


        public static string StrNomServ_WebsSql
        {

            get { return ((NameValueCollection)WebConfigurationManager.GetSection(strSecureAppSettings))[("biomedicina_SOURCE")]; }

        }

        public static string StrBD_WebsSql
        {

            get { return ((NameValueCollection)WebConfigurationManager.GetSection(strSecureAppSettings))[("biomedicina_BD")]; }

        }


        public static string StrUsuario_WebsSql
        {

            get { return ((NameValueCollection)WebConfigurationManager.GetSection(strSecureAppSettings))[("biomedicina_USER")]; }

        }


        public static string StrCnx_WebsSql
        {
            get
            {
                return
              "Data Source=" + StrNomServ_WebsSql +
              ";initial catalog =" + StrBD_WebsSql +
              ";User ID=" + StrUsuario_WebsSql +
              ";Password=" + ((NameValueCollection)WebConfigurationManager.GetSection(strSecureAppSettings))[("biomedicina_PASSWORD")];

            }

            //get
            //{

            //    return "Server=localhost;Database=BIOMEDICINA;Trusted_Connection=True;";

            //}
        }
    }
}