﻿
$(function () {

    limpiar();
    listarUsuario(0);
    listarCliente(0); 
    listarProducto(0);
    listarCategoriaProducto();
	listarEmpresa();
	ListarEmpresaAdm(0);
	ListarCategoriaPro(0)
    //listarTipoServicio();
    //listarUsuario2(0);

    $("#btnCancelar").click(function () {
        //limpiar();
        //$("#RegistrarUsuarioForm").reset();
        $("#RegistrarUsuarioForm").trigger("reset");
        //$("#RegistrarUsuarioForm")[0].reset();

    });

    $("#btnEnviarS").click(function () {


        //if (!$("#RegistrarServicioForm").valid()) {

        //	return false;
        //}


        var pregunta = confirm("¿Desea grabar la información ingresada?");

        if (pregunta)
            //registrarServicio(parametros);
            registrarServicio();


	});

	$("#btnEnviarEmp").click(function () {
		

		if (!$("#RegistrarEmpresaForm").valid()) {

			return false;
		}

		var pregunta = confirm("¿Desea grabar la información ingresada?");
		if (pregunta)
				registrarEmpresa();

	});

	$("#btnEnviarCatP").click(function () {
			   	

	
		registrarCategoriaPro();

	});

    $("#btnEnviar").click(function () {
        

        if (!$("#RegistrarUsuarioForm").valid()) {

            return false;
        }

        var pregunta = confirm("¿Desea grabar la información ingresada?");

        if (pregunta)
            registrarUsuario();

    });

    $("#btnEnviarContraseña").click(function () {
        


        if (!$("#ActualizarConstraseñaForm").valid()) {

            return false;
        }


        var parametros = {};

        parametros["pNumUsuario"] = $("#txtNroUsuario").val();
        parametros["pPassword"] = $("#txtPassword2").val();



        var pregunta = confirm("¿Desea actualizar la contraseña?");

        if (pregunta)
            //registrarUsuario(parametros);
            actualizarContraseña(parametros);

    });

    $("#btnCancelar").click(function () {
        limpiar();
    });

});

//#region Usuario

    function btnRegistrarUsuario() {

        if (!$("#RegistrarUsuarioForm").valid()) {

            return false;
        }
        
        bootbox.confirm({
            title: 'Confirmación',
            message: 'Desea eliminar el usuario?.',
            buttons: {
                confirm: {
                    label: 'Sí',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-link'
                }
            },
            callback: function (result) {

                if (result == true) {
                    var parametros = {};

                    parametros["pNumUsuario"] = numero;
                    parametros["pAccion"] = "E";

                    registrarUsuario(parametros);
                    bootbox.alert({
                        title: 'Confirmation result',
                        message: 'Confirm result: ' + result
                    });

                }


            }
        });

    }

    function limpiar() {

        $("#txtUsuario").val("");
        $("#txtPassword").val("");
        $("#txtNombre").val("");
        $("#txtApPaterno").val("");
        $("#txtApMaterno").val("");
        $("#txtEmail").val("");
        $('#chckActivo').prop('checked', false);
        $(".btnAccion").prop("disabled", false);

    }

    function registrarUsuario() {

        

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/MantenerUsuario",
            data: $("#RegistrarUsuarioForm").serialize(),
            //data: JSON.stringify(parametros),
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            dataType: "json",
            success: function (response) {
                
                var rpta = response.indicador;
                var mensaje = response.contenido;

                if (rpta == "OK") {
                    
                    limpiar();
                    listarUsuario(0);
                    alert(mensaje);
                }
                else
                    alert(mensaje);
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

    }

    function eliminarUsuario(parametros) {

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/EliminarUsuario",
            data: JSON.stringify(parametros),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                
                var rpta = response.indicador;
                var mensaje = response.contenido;

                if (rpta == "OK") {
                    
                    limpiar();
                    listarUsuario(0);
                    //alert(mensaje);
                }
                else
                    alert(mensaje);
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

    function listarUsuario(numero, flag) {

        var parametros = {};
        //parametros["pNroPedido"] = $("#txtNroPedido").val();
        parametros["pNumUsuario"] = numero;
        //parametros["pIdioma"] = $("#txtIdioma").val();

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/ListarUsuario2",
            data: JSON.stringify(parametros),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                
                var rpta = response.indicador;
                var mensaje = response.data;
                console.log(response.data);

                var result = jQuery.parseJSON(response.data);
                console.log(result);
                console.log(result.data);

                if (rpta == "OK") {

                    if (numero == 0) {
                        //$("#tablaUsuarios").html(mensaje);
                        $('#table-users').DataTable({
                            "destroy": true,
                            "data": result,
                            //"ajax": {
                            //    "url": result,
                            //    "dataSrc":""
                            //},
                            "columns": [
                                { "data": "UserName" },
                                { "data": "Nombre" },
                                { "data": "ApPaterno" },
                                { "data": "ApMaterno" },
                                { "data": "Email" },

                                //{
                                //    "render": function (data, type, full, meta) {
                                //        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';
                                //        var activo = "";;
                                //        if (full.Sexo == "M") {

                                //            activo = "Masculino";
                                //        } else {
                                //            activo = "Femenino";
                                //        }

                                //        return activo;
                                //    }
                                //},

                                {
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';
                                        var activo = "";;
                                        if (full.IsActive == true) {

                                            activo = '<span class="badge badge-success">Activo</span>';
                                        } else {
                                            activo = '<span class="badge badge-secondary">Inactivo</span>';
                                        }

                                        return activo;
                                    }
                                },

                                {
                                    "className:": "text-center",
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';

                                        return '<div class="list-icons"><div class="dropdown"><a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu9"></i></a><div class="dropdown-menu dropdown-menu-right">' +
                                            '<a onclick="editarPasajero(' + full.UserId + ')" class="dropdown-item"><i class="icon-pencil7 mr-3 icon-1x"></i> Editar</a>' +
                                            '<a onclick="editarContrasena(' + full.UserId + ')" class="dropdown-item"><i class="icon-file-locked mr-3 icon-1x"></i> Cambiar contraseña</a>' +
                                            '<a onclick="eliminarPasajero(' + full.UserId + ')" class="dropdown-item"><i class="icon-trash-alt mr-3 icon-1x"></i> Eliminar</a>' +
                                            '</div></div></div>';
                                    }
                                }
                                //{
                                //    data: null, render: function (data, type, row) {
                                //        return "<a href='#' class='btn btn-danger' onclick=DeleteData('" + row.CustomerID + "'); >Delete</a>";
                                //    }
                                //},
                            ]

                        });

                    } else {
                        var datos = $.parseJSON(mensaje);

                        if (datos.length > 0) {
                            var fila = datos[0];
                            //var fecha = formatDate(fila.FormatFchNacimiento);

                            if (flag == "EP") {

                                $("#txtUsuario").val(fila.UserName);
                                $("#txtNombre").val(fila.Nombre);
                                $("#txtApPaterno").val(fila.ApPaterno);
                                $("#txtApMaterno").val(fila.ApMaterno);
                                $("#txtEmail").val(fila.Email);

                                if (fila.IsActive == true) {
                                    $('#chckActivo').prop('checked', true);
                                }

                                if (fila.Sexo == "M") {

                                    $("#rdMasculino").prop("checked", true);
                                } else {
                                    $("#rdFemenino").prop("checked", true);

                                }


                            }

                            else {

                                $("#txtUsuario2").val(fila.UserName);

                            }

                            //$("#txtUsuario").val(fila.UserName);
                            //$("#txtNombre").val(fila.Nombre);
                            //$("#txtApPaterno").val(fila.ApPaterno);
                            //$("#txtApMaterno").val(fila.ApMaterno);
                            //$("#txtEmail").val(fila.Email);

                        }

                    }
                }
                else
                    alert(mensaje);
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

        //$('#tablaUsuarios').DataTable({
        //    "paging": true,
        //    "lengthChange": false,
        //    "searching": true,
        //    "ordering": true,
        //    "info": true,
        //    "autoWidth": false,
        //});
    }

    function editarPasajero(numero) {
        
        $("#txtNroUsuario").val(numero);
        $(".btnAccion").prop("disabled", true);
        $("#txtPassword").prop("disabled", true);


        listarUsuario(numero, 'EP');
    }

    function editarContrasena(numero) {
        
        $("#txtNroUsuario").val(numero);
        $(".btnAccion").prop("disabled", true);
        $(".btnPasajero").prop("disabled", true);
        listarUsuario(numero, 'EC');
    }

    function eliminarPasajero(numero) {
        
        //var pregunta = confirm("¿Desea eliminar el pasajero?");

        bootbox.confirm({
            title: 'Confirmación',
            message: 'Desea eliminar el usuario?.',
            buttons: {
                confirm: {
                    label: 'Sí',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-link'
                }
            },
            callback: function (result) {

                if (result == true) {
                    var parametros = {};

                    parametros["pNumUsuario"] = numero;
                    parametros["pAccion"] = "E";

                    eliminarUsuario(parametros);
                    bootbox.alert({
                        title: 'Confirmation result',
                        message: 'Confirm result: ' + result
                    });

                }


            }
        });

        //if (pregunta) {

        //    var parametros = {};

        //    parametros["pNumUsuario"] = numero;
        //    parametros["pAccion"] = "E";

        //    eliminarUsuario(parametros);
        //}

    }

    function actualizarContraseña(parametros) {

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/ActualizarConstraseña",
            data: JSON.stringify(parametros),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                
                var rpta = response.indicador;
                var mensaje = response.contenido;

                if (rpta == "OK") {
                    
                    limpiar();
                    listarUsuario(0);
                    alert(mensaje);
                }
                else
                    alert(mensaje);
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

//#endregion

//#region Cliente
    function listarCliente(numero, flag) {

        var parametros = {};
        //parametros["pNroPedido"] = $("#txtNroPedido").val();
        parametros["pNumCliente"] = numero;
        //parametros["pIdioma"] = $("#txtIdioma").val();

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/ListarCliente",
            data: JSON.stringify(parametros),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                
                var rpta = response.indicador;
                var mensaje = response.data;
                console.log(response.data);

                var result = jQuery.parseJSON(response.data);
                console.log(result);
                console.log(result.data);

                if (rpta == "OK") {

                    if (numero == 0) {
                        //$("#tablaUsuarios").html(mensaje);
                        $('#table-clientes').DataTable({
                            "destroy": true,
                            "data": result,
                            //"ajax": {
                            //    "url": result,
                            //    "dataSrc":""
                            //},
                            "columns": [
                                { "data": "UserName" },
                                { "data": "Nombre" },
                                { "data": "ApPaterno" },
                                { "data": "ApMaterno" },
                                { "data": "Email" },

                                //{
                                //    "render": function (data, type, full, meta) {
                                //        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';
                                //        var activo = "";;
                                //        if (full.Sexo == "M") {

                                //            activo = "Masculino";
                                //        } else {
                                //            activo = "Femenino";
                                //        }

                                //        return activo;
                                //    }
                                //},

                                {
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';
                                        var activo = "";;
                                        if (full.IsActive == true) {

                                            activo = '<span class="badge badge-success">Activo</span>';
                                        } else {
                                            activo = '<span class="badge badge-secondary">Inactivo</span>';
                                        }

                                        return activo;
                                    }
                                },

                                {
                                    "className:": "text-center",
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';

                                        return '<div class="list-icons"><div class="dropdown"><a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu9"></i></a><div class="dropdown-menu dropdown-menu-right">' +
                                            '<a onclick="editarPasajero(' + full.UserId + ')" class="dropdown-item"><i class="icon-pencil7 mr-3 icon-1x"></i> Editar</a>' +
                                            '<a onclick="editarContrasena(' + full.UserId + ')" class="dropdown-item"><i class="icon-file-locked mr-3 icon-1x"></i> Cambiar contraseña</a>' +
                                            '<a onclick="eliminarPasajero(' + full.UserId + ')" class="dropdown-item"><i class="icon-trash-alt mr-3 icon-1x"></i> Eliminar</a>' +
                                            '</div></div></div>';
                                    }
                                },
                                //{
                                //    data: null, render: function (data, type, row) {
                                //        return "<a href='#' class='btn btn-danger' onclick=DeleteData('" + row.CustomerID + "'); >Delete</a>";
                                //    }
                                //},
                            ]

                        });

                    } else {
                        var datos = $.parseJSON(mensaje);

                        if (datos.length > 0) {
                            var fila = datos[0];
                            //var fecha = formatDate(fila.FormatFchNacimiento);

                            if (flag == "EP") {

                                $("#txtUsuario").val(fila.UserName);
                                $("#txtNombre").val(fila.Nombre);
                                $("#txtApPaterno").val(fila.ApPaterno);
                                $("#txtApMaterno").val(fila.ApMaterno);
                                $("#txtEmail").val(fila.Email);

                                if (fila.IsActive == true) {
                                    $('#chckActivo').prop('checked', true);
                                }

                                if (fila.Sexo == "M") {

                                    $("#rdMasculino").prop("checked", true);
                                } else {
                                    $("#rdFemenino").prop("checked", true);

                                }


                            }

                            else {

                                $("#txtUsuario2").val(fila.UserName);

                            }

                            //$("#txtUsuario").val(fila.UserName);
                            //$("#txtNombre").val(fila.Nombre);
                            //$("#txtApPaterno").val(fila.ApPaterno);
                            //$("#txtApMaterno").val(fila.ApMaterno);
                            //$("#txtEmail").val(fila.Email);

                        }

                    }
                }
                else
                    alert(mensaje);
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

        //$('#tablaUsuarios').DataTable({
        //    "paging": true,
        //    "lengthChange": false,
        //    "searching": true,
        //    "ordering": true,
        //    "info": true,
        //    "autoWidth": false,
        //});
    }
//#endregion

//#region Producto

// Setup module
// ------------------------------

var FormWizard = function () {
    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function () {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        // Basic wizard setup
        $('.steps-basic').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            transitionEffect: 'fade',
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Submit form <i class="icon-arrow-right14 ml-2" />'
            },
            onFinished: function (event, currentIndex) {
                registrarProducto()
                //alert('Form submitted.');
            }
        });

        //
        // Wizard with validation
        //

        // Stop function if validation is missing
        if (!$().validate) {
            console.warn('Warning - validate.min.js is not loaded.');
            return;
        }

        // Show form
        var form = $('.steps-validation').show();

        // Initialize wizard
        $('.steps-validation').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Submit form <i class="icon-arrow-right14 ml-2" />'
            },
            transitionEffect: 'fade',
            autoFocus: true,
            onStepChanging: function (event, currentIndex, newIndex) {

                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }

                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex) {

                    // To remove error styles
                    form.find('.body:eq(' + newIndex + ') label.error').remove();
                    form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                form.validate().settings.ignore = ':disabled,:hidden';
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ':disabled';
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                alert('Submitted!');
            }
        });

        // Initialize validation
        $('.steps-validation').validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-invalid-label',
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function (error, element) {

                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo(element.parents('.form-check').parent());
                }

                // Input with icons and Select2
                else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                }

                // Input group, styled file input
                else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                }

                // Other elements
                else {
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    email: true
                }
            }
        });
    };

    // Uniform
    var _componentUniform = function () {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    // Select2 select
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        var $select = $('.form-control-select2').select2({
            minimumResultsForSearch: Infinity,
            width: '100%'
        });

        // Trigger value change when selection is made
        $select.on('change', function () {
            $(this).trigger('blur');
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentWizard();
            _componentUniform();
            _componentSelect2();
        }
    }
}();

// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    FormWizard.init();
});

    function registrarProducto() {

        if (window.FormData !== undefined) {
            //$("#RegistrarProductoForm").serialize()
            var formdata  = new FormData($('form').get(0));


            $.ajax({
                type: "POST",
                url: "/Mantenimiento/MantenerProducto",
                data: formdata,
                //data: JSON.stringify(parametros),
                //contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                //dataType: "json",
                contentType: false, 
                processData: false,  
                success: function (response) {
                    
                    var rpta = response.indicador;
                    var mensaje = response.contenido;

                    if (rpta == "OK") {
                        
                        limpiar();
                        listarProducto(0);
                        alert(mensaje);
                    }
                    else
                        alert(mensaje);
                },
                failure: function (response) {
                    alert(response.responseText);
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });






            //var fileUpload = $("#FichaTecnica").get(0);
            //var files = fileUpload.files;
            //var gg = $("#RegistrarProductoForm").serialize();
            //// Create FormData object  
            //var fileData = new FormData();

            //// Looping over all files and add it to FormData object  
            //for (var i = 0; i < files.length; i++) {
            //    fileData.append('Ficha_tecnica', files[i]);
            //}

            //// Adding one more key to FormData object  
            //fileData.append('username', 'Manas');  


        } else {
            alert("FormData is not supported in the browser.");

        }



  



}

    function listarCategoriaProducto() {

        $.ajax({
            type: "GET",
            url: "/Mantenimiento/ObtenerCategoriaProducto",
            data: "{}",
            success: function (response) {
                
                var data = response.data;
                var s = '<option value="-1">Por favor seleccione una categoria</option>';
                for (var i = 0; i < data.length; i++) {
                    s += '<option value="' + data[i].Id_categoria + '">' + data[i].Nombre_categoria + '</option>';
                }
                $("#ddlCategoriaProducto").html(s);
            }
        });


}

    function listarEmpresa() {

        $.ajax({
            type: "GET",
            url: "/Mantenimiento/ObtenerEmpresa",
            data: "{}",
            success: function (response) {
                
                var data = response.data;
                var s = '<option value="-1">Por favor seleccione una empresa</option>';
                for (var i = 0; i < data.length; i++) {
                    s += '<option value="' + data[i].Id_empresa + '">' + data[i].Razon_social + '</option>';
                }
                $("#ddlEmpresa").html(s);
            }
        });


}

    function listarProducto(numero) {

        var parametros = {};
        //parametros["pNroPedido"] = $("#txtNroPedido").val();
        parametros["pNumProducto"] = numero;
        //parametros["pIdioma"] = $("#txtIdioma").val();

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/ListarProducto",
            data: JSON.stringify(parametros),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                
                var rpta = response.indicador;
                var mensaje = response.data;
                var mensaje2 = response.data2;
                console.log(response.data);

                var result = jQuery.parseJSON(response.data);
                console.log(result);
                console.log(result.data);

                if (rpta == "OK") {

                    if (numero == 0) {
                        $('#table-productos').DataTable({
                            "destroy": true,
                            "data": result,
                            "columns": [
                                { "data": "Id_producto" },
                                { "data": "Nombre" },
                                { "data": "Nombre_empresa" },
                                { "data": "Nombre_categoria" },
                                { "data": "Modelo" },
                                {
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';
                                        var activo = "";;
                                        if (full.IsActive == true) {

                                            activo = '<span class="badge badge-success">Activo</span>';
                                        } else {
                                            activo = '<span class="badge badge-secondary">Inactivo</span>';
                                        }

                                        return activo;
                                    }
                                },

                                {
                                    "className:": "text-center",
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';

                                        return '<div class="list-icons"><div class="dropdown"><a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu9"></i></a><div class="dropdown-menu dropdown-menu-right">' +
                                            '<a onclick="editarProducto(' + full.Id_producto + ')" class="dropdown-item"><i class="icon-pencil7 mr-3 icon-1x"></i> Editar</a>' +
                                            '<a onclick="eliminarProductoBtn(' + full.Id_producto + ')" class="dropdown-item"><i class="icon-trash-alt mr-3 icon-1x"></i> Eliminar</a>' +
                                            '</div></div></div>';
                                    }
                                },
             
                            ]

                        });
                    }
                    else {

                        var datos = $.parseJSON(mensaje);
                        var datos2 = $.parseJSON(mensaje2);
                        
                        if (datos.length > 0) {

                            var fila = datos[0];

                            $("#txtNombre").val(fila.Nombre);
                            $("#txtModelo").val(fila.Modelo);
                            $("#txaDescripcion").val(fila.Descripcion);
                            $("#txtCapacidad").val(fila.Capacidad);
                            $("#txtTemporizador").val(fila.Temporizador);
                            $("#txtTemperatura").val(fila.Temperatura);
                            $("#txtDimension").val(fila.Dimension_tanque);
                            $("#txtPotencia").val(fila.Potencia);
                            $("#txtEnergia").val(fila.Energia);
                            $("#ddlCategoriaProducto").val(fila.Id_categoria);
                            $("#ddlEmpresa").val(fila.Id_empresa);


                  
                        }
                        
                        if (datos2.length > 0) {

                            var fila1 = datos2[0];
                            var fila2 = datos2[1];
                            var fila3 = datos2[2];


                            $('#imagen1').attr('src', fila1.Imagen_url);
                            $('#imagen2').attr('src', fila2.Imagen_url);
                            $('#imagen3').attr('src', fila3.Imagen_url);



                        }

                    }
                }else{
                    alert(mensaje);
            }




            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

        //$('#tablaUsuarios').DataTable({
        //    "paging": true,
        //    "lengthChange": false,
        //    "searching": true,
        //    "ordering": true,
        //    "info": true,
        //    "autoWidth": false,
        //});
}

    function listarUlrImagen(numero) {

        var parametros = {};
        //parametros["pNroPedido"] = $("#txtNroPedido").val();
        parametros["pNumProducto"] = numero;
        //parametros["pIdioma"] = $("#txtIdioma").val();

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/ListarImagenUrl",
            data: JSON.stringify(parametros),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                
                var rpta = response.indicador;
                var mensaje = response.data;
                console.log(response.data);

                var result = jQuery.parseJSON(response.data);
                console.log(result);
                console.log(result.data);

                if (rpta == "OK") {

                    if (numero == 0) {
                        $('#table-productos').DataTable({
                            "destroy": true,
                            "data": result,
                            "columns": [
                                { "data": "Id_producto" },
                                { "data": "Nombre" },
                                { "data": "Nombre_empresa" },
                                { "data": "Nombre_categoria" },
                                { "data": "Modelo" },
                                {
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';
                                        var activo = "";;
                                        if (full.IsActive == true) {

                                            activo = '<span class="badge badge-success">Activo</span>';
                                        } else {
                                            activo = '<span class="badge badge-secondary">Inactivo</span>';
                                        }

                                        return activo;
                                    }
                                },

                                {
                                    "className:": "text-center",
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';

                                        return '<div class="list-icons"><div class="dropdown"><a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu9"></i></a><div class="dropdown-menu dropdown-menu-right">' +
                                            '<a onclick="editarProducto(' + full.Id_producto + ')" class="dropdown-item"><i class="icon-pencil7 mr-3 icon-1x"></i> Editar</a>' +
                                            '<a onclick="eliminarProducto(' + full.Id_producto + ')" class="dropdown-item"><i class="icon-trash-alt mr-3 icon-1x"></i> Eliminar</a>' +
                                            '</div></div></div>';
                                    }
                                },

                            ]

                        });
                    }
                    else {

                        var datos = $.parseJSON(mensaje);
                        
                        if (datos.length > 0) {

                            var fila = datos[0];

                            $("#txtNombre").val(fila.Nombre);
                            $("#txtModelo").val(fila.Modelo);
                            $("#txaDescripcion").val(fila.Descripcion);
                            $("#txtCapacidad").val(fila.Capacidad);
                            $("#txtTemporizador").val(fila.Temporizador);
                            $("#txtTemperatura").val(fila.Temperatura);
                            $("#txtDimension").val(fila.Dimension_tanque);
                            $("#txtPotencia").val(fila.Potencia);
                            $("#txtEnergia").val(fila.Energia);

                            $("#ddlCategoriaProducto").val(fila.Id_categoria);
                            $("#ddlEmpresa").val(fila.Id_empresa);



                        }

                    }
                } else {
                    alert(mensaje);
                }




            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

        //$('#tablaUsuarios').DataTable({
        //    "paging": true,
        //    "lengthChange": false,
        //    "searching": true,
        //    "ordering": true,
        //    "info": true,
        //    "autoWidth": false,
        //});
    }   

    function editarProducto(numero) {
        
        $("#txtIdProducto").val(numero);
        //$(".btnAccion").prop("disabled", true);
        //$("#txtPassword").prop("disabled", true);
        listarProducto(numero);
    }

    function eliminarProducto(parametros) {

    $.ajax({
        type: "POST",
        url: "/Mantenimiento/EliminarProducto",
        data: JSON.stringify(parametros),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var rpta = response.indicador;
            var mensaje = response.contenido;

            if (rpta == "OK") {

                limpiar();
                listarProducto(0);
                //alert(mensaje);
            }
            else
                alert(mensaje);
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}

    function eliminarProductoBtn(numero) {

        //var pregunta = confirm("¿Desea eliminar el pasajero?");

        bootbox.confirm({
            title: 'Confirmación',
            message: 'Desea eliminar el producto?.',
            buttons: {
                confirm: {
                    label: 'Sí',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-link'
                }
            },
            callback: function (result) {

                if (result == true) {
                    var parametros = {};

                    parametros["pNumProducto"] = numero;

                    eliminarProducto(parametros);
                    bootbox.alert({
                        title: 'Confirmation result',
                        message: 'Confirm result: ' + result
                    });

                }


            }
        });


    }
//#endregion



/*Inicio Servicio*/
function registrarServicio() {

    $.ajax({
        type: "POST",
        url: "/Mantenimiento/MantenerServicio",
        data: $("#RegistrarServicioForm").serialize(),
        //data: JSON.stringify(parametros),
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        dataType: "json",
        success: function (response) {
            
            var rpta = response.indicador;
            var mensaje = response.contenido;

            if (rpta == "OK") {     
                limpiar();
                //listarServicio(0);
            }
            else
                alert(mensaje);
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });

}


//#region Empresa

    function editarEmpresas(numero) {
	    
	    $("#txtNroEmpresa").val(numero);
	    $(".btnAccion").prop("disabled", true);
	    $(".btnPasajero").prop("disabled", true);
	    ListarEmpresaAdm(numero, 'EP');
    }

    function eliminarEmpresas(numero) {
	    
	    //var pregunta = confirm("¿Desea eliminar el Empresa?");

	    bootbox.confirm({
		    title: 'Confirmación',
		    message: 'Desea inactivar la empresa?.',
		    buttons: {
			    confirm: {
				    label: 'Sí',
				    className: 'btn-primary'
			    },
			    cancel: {
				    label: 'Cancelar',
				    className: 'btn-link'
			    }
		    },
		    callback: function (result) {

			    if (result == true) {
				    var parametros = {};

				    parametros["pNumEmpresa"] = numero;
				    //parametros["pAccion"] = "P";

				    eliminarEmpresa(parametros);
				    bootbox.alert({
					    title: 'Confirmation result',
					    message: 'Confirm result: ' + result
				    });

			    }


		    }
	    });

	    //if (pregunta) {

	    //    var parametros = {};

	    //    parametros["pNumUsuario"] = numero;
	    //    parametros["pAccion"] = "E";

	    //    eliminarUsuario(parametros);
	    //}

    }

    function eliminarEmpresa(parametros) {
	    
	    $.ajax({
		    type: "POST",
		    url: "/Mantenimiento/EliminarEmpresa",
		    data: JSON.stringify(parametros),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
		    success: function (response) {
			    
			    var rpta = response.indicador;
			    var mensaje = response.contenido;

			    if (rpta == "OK") {
				    
				    limpiar();
				    ListarEmpresaAdm(0);
				    //alert(mensaje);
			    }
			    else
				    alert(mensaje);
		    },
		    failure: function (response) {
			    alert(response.responseText);
		    },
		    error: function (response) {
			    alert(response.responseText);
		    }
	    });
    }

    function ListarEmpresaAdm(numero) {

	    var parametros = {};
	    //parametros["pNroPedido"] = $("#txtNroPedido").val();
        parametros["pNumEmpresa"] = numero;
	    //parametros["pIdioma"] = $("#txtIdioma").val();

	    $.ajax({
		    type: "POST",
		    url: "/Mantenimiento/ListarEmpresaAdm",
		    data: JSON.stringify(parametros),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
		    success: function (response) {
			    
			    var rpta = response.indicador;
			    var mensaje = response.data;
			    console.log(response.data);

			    var result = jQuery.parseJSON(response.data);
			    console.log(result);
			    console.log(result.data);

			    if (rpta == "OK") {

				    if (numero == 0) {
					    //$("#tablaUsuarios").html(mensaje);
					    $('#table-empresas').DataTable({
						    "destroy": true,
						    "data": result,

						    "columns": [
							    { "data": "Id_empresa" },
							    { "data": "Razon_social" },
							    { "data": "RUC" },
							    { "data": "Descripcion" },
							    { "data": "Direccion" },
							    { "data": "Inicio_Op" },

							    {
								    "render": function (data, type, full, meta) {
									    //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';
									    var activo = "";;
									    if (full.IsActive == true) {

										    activo = '<span class="badge badge-success">Activo</span>';
									    } else {
										    activo = '<span class="badge badge-secondary">Inactivo</span>';
									    }

									    return activo;
								    }
							    },

							    {
								    "className:": "text-center",
								    "render": function (data, type, full, meta) {
									    //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';

									    return '<div class="list-icons"><div class="dropdown"><a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu9"></i></a><div class="dropdown-menu dropdown-menu-right">' +
										    '<a onclick="editarEmpresas(' + full.Id_empresa + ')" class="dropdown-item"><i class="icon-pencil7 mr-3 icon-1x"></i> Editar</a>' +
										    '<a onclick="eliminarEmpresas(' + full.Id_empresa + ')" class="dropdown-item"><i class="icon-trash-alt mr-3 icon-1x"></i> Eliminar</a>' +
										    '</div></div></div>';
								    }
							    },

						    ]

					    });

				    } else {
					    var datos = $.parseJSON(mensaje);

					    if (datos.length > 0) {
						    var fila = datos[0];
						    //var fecha = formatDate(fila.FormatFchNacimiento);

						  

							    $("#txtNroEmpresa").val(fila.Id_empresa);
							    $("#txtNombre").val(fila.Razon_social);
							    $("#txtDesEmpresa").val(fila.Descripcion);
							    $("#txtRUC").val(fila.RUC);
							    $("#txtDireccion").val(fila.Direccion);
							    $("#txtInicio_Op").val(fila.Inicio_Op);						
						   

						

					    }

				    }
			    }
			    else
				    alert(mensaje);
		    },
		    failure: function (response) {
			    alert(response.responseText);
		    },
		    error: function (response) {
			    alert(response.responseText);
		    }
	    });

    }

    function registrarEmpresa() {

	   
	    $.ajax({
		    type: "POST",
		    url: "/Mantenimiento/MantenerEmpresa",
		    data: $("#RegistrarEmpresaForm").serialize(),
		    //data: JSON.stringify(parametros),
		    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		    dataType: "json",
		    success: function (response) {
			    
			    var rpta = response.indicador;
			    var mensaje = response.contenido;

			    if (rpta == "OK") {
				    
				    ListarEmpresaAdm();
				    alert(mensaje);
			    }
			    else
				    alert(mensaje);
		    },
		    failure: function (response) {
			    alert(response.responseText);
		    },
		    error: function (response) {
			    alert(response.responseText);
		    }
	    });

    }

//#endregion

//#region Categoria producto

    function registrarCategoriaPro() {

        

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/MantenerCatPro",
            data: $("#RegistrarCatProForm").serialize(),
            //data: JSON.stringify(parametros),
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            dataType: "json",
            success: function (response) {
                
                var rpta = response.indicador;
                var mensaje = response.contenido;

                if (rpta == "OK") {
                    
                    ListarCategoriaPro(0);
                    alert(mensaje);
                }
                else
                    alert(mensaje);
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

    }

    function ListarCategoriaPro(numero) {

        var parametros = {};
        //parametros["pNroPedido"] = $("#txtNroPedido").val();
        parametros["pNumCategoria"] = numero;
        //parametros["pIdioma"] = $("#txtIdioma").val();

        $.ajax({
            type: "POST",
            url: "/Mantenimiento/ListarCategoriaPro",
            data: JSON.stringify(parametros),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger
                var rpta = response.indicador;
                var mensaje = response.data;
                console.log(response.data);

                var result = jQuery.parseJSON(response.data);
                console.log(result);
                console.log(result.data);

                if (rpta == "OK") {

                    if (numero == 0) {
                        //$("#tablaUsuarios").html(mensaje);
                        $('#table-catPro').DataTable({
                            "destroy": true,
                            "data": result,

                            "columns": [
                                { "data": "Id_categoria" },
                                { "data": "Nombre_categoria" },

                                {
                                    "render": function (data, type, full, meta) {
                                        var activo = "";;
                                        if (full.IsActive == true) {

                                            activo = '<span class="badge badge-success">Activo</span>';
                                        } else {
                                            activo = '<span class="badge badge-secondary">Inactivo</span>';
                                        }

                                        return activo;
                                    }
                                },

                                {
                                    "className:": "text-center",
                                    "render": function (data, type, full, meta) {
                                        //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';

                                        return '<div class="list-icons"><div class="dropdown"><a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu9"></i></a><div class="dropdown-menu dropdown-menu-right">' +
                                            '<a onclick="editarCategoriaPro(' + full.Id_categoria + ')" class="dropdown-item"><i class="icon-pencil7 mr-3 icon-1x"></i> Editar</a>' +
                                            '<a onclick="eliminarCategoriaPro(' + full.Id_categoria + ')" class="dropdown-item"><i class="icon-trash-alt mr-3 icon-1x"></i> Eliminar</a>' +
                                            '</div></div></div>';
                                    }
                                },

                            ]

                        });

                    } else {
                        var datos = $.parseJSON(mensaje);

                        if (datos.length > 0) {
                            var fila = datos[0];
                            //var fecha = formatDate(fila.FormatFchNacimiento);

                           

                            $("#txtNombreCat").val(fila.Nombre_categoria);
                        
                          
                       

                        }

                    }
                }
                else
                    alert(mensaje);
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

}

    function editarCategoriaPro(numero) {

            $("#txtNroCat").val(numero);
            //$(".btnAccion").prop("disabled", true);
            //$("#txtPassword").prop("disabled", true);
            ListarCategoriaPro(numero);
        }
//#endregion




